/*
 *Jarom Buys
 * 4/25/2018
 * CSCI 13 - Online
 * Homework 13
 * Model 
 * Get weahter data from Weather Underground as Json and display current conditions in GUI using hxml.
 */
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URLEncoder;
import java.net.URL;
import javafx.scene.image.Image;

import com.google.gson.*;

public class WxModel
{
	JsonElement jse = null;
		
	public boolean getWx(String zip)
	{
		String apiKey = "5610806bfa9747bf";
		//String wxReport = null;
		
		// http://api.wunderground.com/api/5610806bfa9747bf/conditions/q/

		try
		{
			// Construct WxStation API URL
			URL wxURL = new URL("http://api.wunderground.com/api/" + apiKey + 
			"/conditions/q/" + zip + ".json");

			// Open the URL
			InputStream is = wxURL.openStream(); // throws an IOException
			BufferedReader br = new BufferedReader(new InputStreamReader(is));

			// Read the result into a JSON Element
			jse = new JsonParser().parse(br);
      
			// Close the connection
			is.close();
			br.close();
		}
		catch (java.io.UnsupportedEncodingException uee)
		{
			uee.printStackTrace();
		}
		catch (java.net.MalformedURLException mue)
		{
			mue.printStackTrace();
		}
		catch (java.io.IOException ioe)
		{
			ioe.printStackTrace();
		}
		
		return isValid();
	}
	
	public boolean isValid()
	{
	
		// try got get "error" from json data; if zipcode is invalid, error will exist
		try
		{
			String err = jse.getAsJsonObject().get("response")
							.getAsJsonObject().get("error")
							.getAsJsonObject().get("description").getAsString();
			return false;
		}
		
		catch (java.lang.NullPointerException npe)
		{
			// if NullPointerException is caught, then "error" doesn't exist in the data
			// so zipcode is valid
			return true;
		}
	}
		
	public String getLocation()
	{
		// Build a weather report
		return jse.getAsJsonObject().get("current_observation")
					.getAsJsonObject().get("display_location")
					.getAsJsonObject().get("full").getAsString();
	}
		
	public String getTime()
	{
		return jse.getAsJsonObject().get("current_observation")
					.getAsJsonObject().get("observation_time").getAsString();
	}
		
	public String getWeather()
	{
		return jse.getAsJsonObject().get("current_observation")
                      .getAsJsonObject().get("weather").getAsString();
	}
		
	public Double getTemp()
	{
		return jse.getAsJsonObject().get("current_observation")
                      .getAsJsonObject().get("temp_f").getAsDouble();
	}
		
	public String getWind()
	{
		return jse.getAsJsonObject().get("current_observation")
					.getAsJsonObject().get("wind_string").getAsString();
	}
	
	public String getVisibility()
	{
		return jse.getAsJsonObject().get("current_observation")
                      .getAsJsonObject().get("visibility_mi").getAsString();
	}
		
	public String getPressure()
	{
		return jse.getAsJsonObject().get("current_observation")
                      .getAsJsonObject().get("pressure_in").getAsString();
	}
	
	public Image getImage()
	{
		String iconURL = jse.getAsJsonObject().get("current_observation").getAsJsonObject().get("icon_url").getAsString();
		return new Image(iconURL);
	}
	
	public static boolean isInteger(String s) 
	{
		try 
		{ 
			Integer.parseInt(s); 
		} 
		catch(NumberFormatException e) 
		{ 
			return false; 
		} 
		catch(NullPointerException e) 
		{
			return false;
		}
		// only got here if we didn't return false
		return true;
	}
}
	

/*	public static void main(String[] args)
	{
    Wxhw b = new Wxhw();
	System.out.println("args length: " + args[0] + ": " + args.length);
      
    if ( args.length == 0 )
	  System.out.println("Please enter a valid Zip Code as the first argument.");
    else if ( args[0].length() != 5 )
	  System.out.println("ERROR: Zip Code must be 5 digits");
	else if ( !Wxhw.isInteger(args[0]) ) 
	  System.out.println("ERROR: Please enter a valid Zip Code");
    else
	{
		  String wx = b.getWx(args[0]);
      if ( wx != null )
		    System.out.println(wx);
    }
	}
}*/
