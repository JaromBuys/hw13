/*
 * Jarom Buys
 * 4/25/2018
 * CSCI 13 - Online
 * Homework 13
 *
 * Main - This code initiates the View and Controller.
 * Get weahter data from Weather Underground as Json and display current conditions in GUI using hxml.
 */
 
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class WxMain extends Application 
{
    
    @Override
    public void start(Stage stage) throws Exception 
	{
        Parent root = FXMLLoader.load(getClass().getResource("./WxView.fxml"));
        
        Scene scene = new Scene(root);
        
        stage.setScene(scene);
        stage.setTitle("Wx Jarom Buys");
        stage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) 
	{
        launch(args);
    }
    
}
