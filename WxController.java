/*
 * Jarom Buys
 * 4/25/2018
 * CSCI 13 - Online
 * Homework 13
 *
 * Controller - This is the controller for the FXML document that contains the view. 
 * Get weather data from Weather Underground as Json and display current conditions in GUI using hxml.
 */
 
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;


public class WxController implements Initializable {

  @FXML
  private Button btngo;

  @FXML
  private TextField txtzipcode;

  @FXML
  private Label lblcity;

  @FXML
  private Label lbltemp;
  
  @FXML
  private Label lbltime;
  
  @FXML
  private Label lblweather;
  
  @FXML
  private Label lblwind;
  
  @FXML
  private Label lblpressure;
  
  @FXML
  private Label lblvisibility;
  
  @FXML
  private ImageView iconwx;

  @FXML
  private void handleButtonAction(ActionEvent e) {
	// Create object to access the Model
	WxModel weather = new WxModel();

    // Has the go button been pressed?
    if (e.getSource() == btngo)
    {
		String zipcode = txtzipcode.getText();
		if (weather.getWx(zipcode))
		{
			lblcity.setText(weather.getLocation());
			lbltemp.setText(String.valueOf(weather.getTemp()));
			iconwx.setImage(weather.getImage());
			lblpressure.setText(weather.getPressure());
			lbltime.setText(weather.getTime());
			lblvisibility.setText(weather.getVisibility());
			lblweather.setText(weather.getWeather());
			lblwind.setText(weather.getWind());
		}
		else
		{
			lblcity.setText("Invalid Zipcode");
			lbltemp.setText("");
			iconwx.setImage(new Image("error.jpg"));
			lblpressure.setText("");
			lbltime.setText("");
			lblvisibility.setText("");
			lblweather.setText("");
			lblwind.setText("");
		}
    }
  }

  @Override
  public void initialize(URL url, ResourceBundle rb) {
    // TODO
  }    

}
